<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class Reg extends BaseController
{
    private function genCode(){
        $code = Str::random(4);
        session(['validate_code'=>$code]);
        return $this->genImageFromString($code);
    }

    private function genImageFromString($string){
        $font = 6;
        $width = imagefontwidth($font) * strlen($string);
        $height = imagefontheight($font);

        $image = imagecreate($width, $height);
        $backgroundColor = imagecolorallocate($image, 255, 255, 255);
        $textColor = imagecolorallocate($image, 0, 0, 0);
        imagestring($image, $font, 0, 0, $string, $textColor);

        ob_start();
        imagepng($image);
        $encodedImage = base64_encode(ob_get_clean());
        imagedestroy($image);

        return $encodedImage;
    }

    public function page(){
        return view('reg.page',[
            'code' => $this->genCode()
        ]);
    }

    public function send(Request $request){
        $validator = Validator::make($request->all(),[
            'orgname' => 'required|regex:/^[a-zA-Zа-яА-ЯЁёӘәҒғҚқҢңӨөҰұҮүҺһІі0-9«»,.\-#№@&()]+$/u',
            'address' => 'required|regex:/^[a-zA-Zа-яА-ЯЁёӘәҒғҚқҢңӨөҰұҮүҺһІі0-9«»,.\-#№@&()]+$/u',
            'postcode' => 'required|digits:6',
            'phone' => 'required|digits:11',
            'email' => 'required|email',
            'bin' => 'required|digits:8',
            'iikkz' => 'required|size:20',
            'bank' => 'required',
            'bik' => 'required|size:8',
            'supervisor' => 'required',
            'respperson' => 'required',
            'respperson_phone' => 'required|digits:11',
            'respperson_email' => 'required|email',
            'domain' => 'required',
            'document' => 'required|mimes:pdf|max:2048',
            'code' => 'required'
        ]);

        if($validator->fails()){
            $validator->errors()->add('newCode',$this->genCode());
            return response()->json($validator->errors());
        }

        $validateCode = session('validate_code');

        if($validateCode != $request->code){
            $validator->errors()->add('newCode',$this->genCode());
            $validator->errors()->add('code','Неверный код');
            return response()->json($validator->errors());
        }

        $uploadPath = $request->file('document')->store('documents','public');

        $fields = $request->except('code','document');
        $fields['document'] = $uploadPath;

        User::create($fields);

        unset($fields['document']);

        Mail::send('reg.mail',$fields, function($message) use($uploadPath){
            $message->to('karina@corp.mail.kz')->subject('Форма регистрации с edu.kz');
            $message->attach(env('APP_URL').'/'.$uploadPath);
        });

        return response()->json(['status'=>'Заявка отправлена']);
    }
}
