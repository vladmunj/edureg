function ajax($url,$data,callback){
	let $csrf = $('meta[name="csrf-token"]').attr('content');

	$.ajax({
		url: $url,
		data: $data,
		headers: {
            'X-CSRF-Token' : $csrf
        },
        type: "POST",
        contentType:false,
        processData: false,
		success: function($response){
            callback($response);
        }
	});
}

$(function(){
	$(document).on('submit','.regForm',function(){
		let $form = $(this);
		let $data = new FormData(this);
		let $submit = $form.find('[type="submit"]');

		$submit.attr('disabled','disabled');

		ajax('/send',$data,function($response){
			$('.error').text('');
			$submit.removeAttr('disabled');

			$form.find('.input-fields input[type="text"]').css('border','1px solid green');
			$form.find('.input-fields input[type="file"]').css('border','none');

			if(typeof $response.status === 'undefined'){
				
				for(const [$input,$error] of Object.entries($response)){
					$form.find('.input-fields input[name="'+$input+'"]').css('border','1px solid red');
					$form.find('.input-fields input[name="'+$input+'"]').siblings('.error').text($error);
				}

				$form.find('.code').attr('src','data:image/png;base64,'+$response.newCode);

				// $('html,body').stop().animate({ scrollTop: $form.offset().top }, 1000);

				return;
			}

			$form.find('.result').text($response.status);
			$form[0].reset();
		});
		return false;
	});
});