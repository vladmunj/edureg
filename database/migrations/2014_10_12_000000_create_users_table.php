<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('orgname');
            $table->string('address');
            $table->string('postcode');
            $table->string('phone');
            $table->string('email');
            $table->string('bin');
            $table->string('iikkz');
            $table->string('bank');
            $table->string('bik');
            $table->string('supervisor');
            $table->string('respperson');
            $table->string('respperson_phone');
            $table->string('respperson_email');
            $table->string('domain');
            $table->string('document');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
