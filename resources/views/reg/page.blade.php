<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dev/css/reg.css') }}">
	<title>Регистрация</title>
</head>
<body>
	<div class="container">
		<div class="col-lg-12">
			<div class="wrapper">
				<h1>Форма для регистрации домена в зоне EDU.KZ для юридических лиц</h1>
				<form method="POST" class="regForm" enctype="multipart/form-data">
					<div class="input-fields">
						<div class="row">
							<div class="col">
								<input type="text" name="orgname" placeholder="Наименование организации (пропишите полностью)">
								<span class="error"></span>
							</div>
							<div class="col">
								<input type="text" name="address" placeholder="Юридический адрес организации, включая город/область">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<input type="text" name="postcode" placeholder="Почтовый индекс (6 цифр)">
								<span class="error"></span>
							</div>
							<div class="col">
								<input type="text" name="phone" placeholder="Телефон организации">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<input type="text" name="email" placeholder="E-mail организации">
								<span class="error"></span>
							</div>
							<div class="col">
								<input type="text" name="bin" placeholder="БИН организации (8 цифр)">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<input type="text" name="iikkz" placeholder="ИИК KZ (20 значный счет)">
								<span class="error"></span>
							</div>
							<div class="col">
								<input type="text" name="bank" placeholder="Наименование банка">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<input type="text" name="bik" placeholder="БИК (8 латинских букв и цифр)">
								<span class="error"></span>
							</div>
							<div class="col">
								<input type="text" name="supervisor" placeholder="ФИО руководителя организации">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<input type="text" name="respperson" placeholder="Ответственное лицо">
								<span class="error"></span>
							</div>
							<div class="col">
								<input type="text" name="respperson_phone" placeholder="Телефон ответственного лица">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<input type="text" name="respperson_email" placeholder="E-mail ответственного лица">
								<span class="error"></span>
							</div>
							<div class="col">
								<input type="text" name="domain" placeholder="Желаемое название доменного имени в зоне edu.kz">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label>Свидетельство о государственной регистрации организации</label>
								<input type="file" name="document" accept="application/pdf">
								<span class="error"></span>
							</div>
						</div>
						<div class="row">
							<div class="col code-block">
								<img src="data:image/png;base64,{{ $code }}" class="code" />
								<input type="text" name="code" placeholder="Введите код с картинки">
								<span class="error"></span>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col">
							<input type="submit" value="Отправить" class="btn btn-primary">
							<span class="result"></span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{ asset('dev/js/jquery-3.6.0.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('dev/js/reg.js') }}"></script>
</body>
</html>